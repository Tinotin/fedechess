# Fede Chess (Chess Federation Administration)



## Programa de administracion de federados y competiciones

Este programa va a intentar gestionar el alta, modificación y eliminación de los federados. 

Por otra parte, también la gestión de los órganos y personas de la Federación.

En un principio se va a orientar a las federaciones de Ajedrez, pero también se podrá adaptar a oto tipo de Federaciones.

Se escribirá la documentación en Español, pero los comandos, variables y demás en Ingles para una mayor compresión e unívocos.

Como se va a permitir contribuciones. Se deja las recomendaciones de Gitlab para que se pueda incorporar mas desarrolladores. A su vez tambien personas que puedan contribuir por su experiencia en las diferentes secciones del proyecto a nivel documental.

## Programacion

Se va a realizar en Rust. Posteriormente se vera cuales seran los frontends y backends mas apropiados.  


```sql
La base de datos se utilizara en MariaDB.
Las alternativas MySQL, Postgress, MongoDb, etc.
```

Crearemos una base de datos ficticios para las pruebas. Los datos los cogeremos de los datos publicos y accesibles. Los privados se generaran de forma aleatoria.
Ejemplo son los Numeros de Documentos, Direcciones Telefonoes y emails. Todo aquellos que la LPRD nos obliga como programadores para entorno de pruebas.




